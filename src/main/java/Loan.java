public class Loan {
    private int amount;
    private boolean isPaid;
    private String borrower;
    private double interestRate;

    public Loan(int amount, boolean isPaid, String borrower, double interestRate) {
        this.amount = amount;
        this.isPaid = isPaid;
        this.borrower = borrower;
        this.interestRate = interestRate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }
}
